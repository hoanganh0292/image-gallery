import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalComponent} from './modal.component'

/*
  Modal module that encapsulates the modal component so it can be imported by the app module
*/
@NgModule({
  declarations: [ModalComponent],
  imports: [CommonModule],
  exports: [ModalComponent]
})
export class ModalModule { }
