import { Component, OnInit, ViewEncapsulation, ElementRef,Input,OnDestroy } from '@angular/core';
import {ModalService} from './modal.service';
import {ImageInfo} from '../imageinfo';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit, OnDestroy {
  @Input() id:string;
  @Input() image: ImageInfo;
  private element: any;
  constructor(private modalService: ModalService, private el: ElementRef) { 
    this.element = el.nativeElement;
  }

  ngOnInit(): void {
    if (!this.id)
    {
      console.error('modal must have an id');
      return;
    }

    //move element to bottom of page (just before </body>)
    document.body.appendChild(this.element);

    this.element.addEventListener('click', el => {
      if (el.target.className === 'app-modal'){
        this.close();
      }
    });

    this.modalService.add(this);
  }

  ngOnDestroy() {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  open(): void{
    this.element.style.display = 'block';    
    document.body.classList.add('app-modal-open');
  }

  close(): void{
    this.element.style.display = 'none';
    document.body.classList.remove('app-modal-open')
  }

}
