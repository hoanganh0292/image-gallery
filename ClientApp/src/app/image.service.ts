import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpEventType, HttpHeaders, HttpEvent } from '@angular/common/http';
import {Observable, of} from 'rxjs';
import { ImageInfo } from './imageinfo';
import { MessageService } from './message.service';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private baseAPIUrl: string;
  private httpOptions = {   
    reportProgress:true
  };  
  constructor(private httpClient: HttpClient, 
    @Inject('BASE_URL') baseUrl: string,
    private messageService: MessageService) 
  { 
    this.baseAPIUrl= baseUrl + 'api/image';
  }

  getImages(): Observable<ImageInfo[]> {
    return this.httpClient.get<ImageInfo[]>(this.baseAPIUrl)
           .pipe(
              tap(_ => this.log('get list image')),
              catchError(this.handleError<ImageInfo[]>('getImages',[]))
           );    
  }

  getImageByKeyword(keyword: string): Observable<ImageInfo[]> {
    return this.httpClient.get<ImageInfo[]>(this.baseAPIUrl + `/${keyword}`)
            .pipe(
              tap(_ => this.log('get list image by keyword')),
              catchError(this.handleError<ImageInfo[]>('getImageByKeyWord', []))
            );
  }
  private handleError<T> (operation = 'operation', result?: T)
  {
    return (error:any): Observable<T> => {
      console.error("Server error: " + error.status + "=====" + error.message);
      console.error("Client error: " + error.error + "=====" + error.error.message);      
      this.log(`${operation} failed: ${error.message}`);
      return of (result as T);
    }
  }
  private log(message:string)
  {
    this.messageService.add(`Image Service: ${message}`);
  }

  
  uploadImage(image: Blob): Observable<HttpEvent<void>>{
    const formData = new FormData();
    formData.append('img', image);    
    // return this.httpClient.post<HttpEvent<void>>(this.baseAPIUrl, formData, this.httpOptions)
    //   .pipe(
    //   tap(_ => this.log(`Add image with title = ${image}`)),
    //   catchError(this.handleError<HttpEvent<void>>('uploadImage'))      
    // );
    return this.httpClient.request(new HttpRequest('POST', this.baseAPIUrl, formData, this.httpOptions));
  }

  renameImage(oldName: string, newName: string): Observable<any>  {
    // return this.httpClient.put(this.baseAPIUrl, new Object({OldName: oldName, NewName: newName}), this.httpOptions)
    //                       .pipe(
    //                         tap(_ => this.log('update image with new name' + newName)),
    //                         catchError(this.handleError<any>('renameImage'))
    //                       );
    return this.httpClient.request(new HttpRequest('PUT', this.baseAPIUrl, new Object({OldName: oldName, NewName: newName}), this.httpOptions));                
  }

  deleteImage(imageName: string){
    return this.httpClient.request(new HttpRequest('DELETE', this.baseAPIUrl, new Object({ImageName: imageName}), this.httpOptions));                
  }


}
