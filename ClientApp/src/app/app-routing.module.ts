import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes, Router} from '@angular/router';
import {HomeComponent} from './home/home.component';
import { ImageDetailComponent } from "./image-detail/image-detail.component";


const routes: Routes = [  
  {path:'heroes', component:ImageDetailComponent},
  {path:'dashboard', component:HomeComponent},  
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
