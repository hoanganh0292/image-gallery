export class ImageInfo {
    Path: string;
    Title: string;
    NameWithExtension: string;
    constructor(image:any){
      this.Path = image.Path;
      this.Title = image.Title;
      this.NameWithExtension = image.NameWithExtension;
    }    
  }