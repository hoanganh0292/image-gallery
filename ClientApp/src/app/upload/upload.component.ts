import { Component, OnInit, Input,Output,EventEmitter, ViewChild, ElementRef } from '@angular/core';
import {HttpEventType } from '@angular/common/http';
import {ProgressStatus, ProgressStatusEnum } from '../model/progress-status.model';
import { ImageService } from '../image.service';
import { ImageInfo } from '../imageinfo';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  @Input() public disable: boolean;
  @Output() public uploadStatus: EventEmitter<ProgressStatus>;
  @ViewChild('inputFile', {static:true}) inputFile: ElementRef;

  @Output() public searchByKeyword: EventEmitter<string>;    
  modelChanged: Subject<string> = new Subject<string>();

  constructor(private imageService: ImageService) { 
    this.uploadStatus = new EventEmitter<ProgressStatus>();
    this.searchByKeyword = new EventEmitter<string>();
    this.modelChanged.pipe(
        debounceTime(300), // wait 300ms after the last event before emitting last event
        distinctUntilChanged() // only emit if value is different from previous value
    ).subscribe(searchValue => this.searchByKeyword.emit(searchValue));      
  }
  ngOnInit() {
    
  }

  ngDocheck(){
    console.log('change');
  }
  public upload(event){
    if (event.target.files && event.target.files.length > 0)
    {
      const formData = new FormData();
      const img = event.target.files[0];
      this.uploadStatus.emit({status: ProgressStatusEnum.START});      
      this.imageService.uploadImage(img).subscribe(
        data => {          
          if (data) {
            switch (data.type){
              case HttpEventType.UploadProgress:
                this.uploadStatus.emit({status:ProgressStatusEnum.IN_PROGRESS, percentage:Math.round((data.loaded/data.total) * 100)});
                break;
              case HttpEventType.Response:
                this.inputFile.nativeElement.value = '';                                
                this.uploadStatus.emit({status: ProgressStatusEnum.COMPLETE, imageInfo: new ImageInfo(data.body)});
                break;
            }
          }
        },
        error => {
          this.inputFile.nativeElement.value = '';
          this.uploadStatus.emit({status: ProgressStatusEnum.ERROR});
        }
      );      
    }        
  }
  
  onSearchChange(searchValue: string) {    
    this.modelChanged.next(searchValue);    
  }

  
}
