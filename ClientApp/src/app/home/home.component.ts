import { Component, Inject, OnInit } from '@angular/core';
import { ImageInfo } from '../imageinfo';
import { ImageService } from '../image.service';
import { ProgressStatus, ProgressStatusEnum } from '../model/progress-status.model';
import { ModalService } from '../modal/modal.service';
import {HttpEventType } from '@angular/common/http';
@Component({
  selector: 'app-home', 
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit  {
  private listImage: ImageInfo[];
  private selectedImage: ImageInfo;
  private newImageName: string;
  private currentModalId:string
  public showProgress: boolean;
  public percentage: number;
  public showUploadError: boolean;


  constructor(private imageService:ImageService, private modalService: ModalService ) {    
  }

  ngOnInit(){
    this.getImages();
  }

  getImages(): void {
    this.imageService.getImages().subscribe(images => {this.listImage = images;});
  }

  public uploadStatus (event: ProgressStatus)
  {
    switch (event.status)
    {
      case ProgressStatusEnum.START:
        this.showProgress= false;
        break;
      case ProgressStatusEnum.IN_PROGRESS:
        this.showProgress= true;
        console.log(`${event.percentage}%`)
        this.percentage = event.percentage;
        break;
      case ProgressStatusEnum.COMPLETE:
        this.showProgress= false;
        console.log(event.imageInfo);
        this.listImage.unshift(event.imageInfo);
        break;
      case ProgressStatusEnum.ERROR:
        this.showProgress= false;
        this.showUploadError = true;
        break;
    }
  }

  openModal(id:string, selectedImg:ImageInfo){        
    this.modalService.open(id);   
    this.currentModalId = id;  
    this.selectedImage = selectedImg; 
    this.newImageName = selectedImg.Title; 
  }

  closeModal(id:string){
    this.modalService.close(id);
  }

  searchByKeyword (keyword: string)
  {
    this.listImage = [];
    this.imageService.getImageByKeyword(keyword)
                     .subscribe(images => this.listImage = images);
  }
  renameImage()
  {
    if(this.selectedImage.Title !== this.newImageName)
    {      
      this.imageService.renameImage(this.selectedImage.NameWithExtension, this.newImageName)  
                       .subscribe(
                          data => {
                                    if (data && data.type === HttpEventType.Response)
                                    {                                              
                                      this.selectedImage.NameWithExtension = this.selectedImage.NameWithExtension.replace(this.selectedImage.Title, this.newImageName);
                                      this.selectedImage.Title = this.newImageName;                                      
                                      this.modalService.close(this.currentModalId);
                                    }                                            
                                  },
                          error => {
                            window.alert(error.error.message);                                    
                          });
      
    } 
    else
    {
      this.modalService.close(this.currentModalId);
    }    
  }

  close()
  {
    this.modalService.close(this.currentModalId);
  }

  deleteImage()
  {
    if(window.confirm("Do you want delete this image?"))
    {      
      this.imageService.deleteImage(this.selectedImage.NameWithExtension)
                        .subscribe(
                          data => {
                          if (data && data.type === HttpEventType.Response)
                          {                                              
                            this.listImage.splice(this.listImage.indexOf(this.selectedImage),1);
                            this.modalService.close(this.currentModalId);
                          }                                            
                        },
                error => {
                  window.alert(error.error.message);                                    
                });
    }

    
  }

  
}

