import { Component, OnInit } from '@angular/core';
import { ImageInfo } from '../imageinfo';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.css']
})
export class ImageDetailComponent implements OnInit {  
  private imagePath: string;
  constructor(
    private route: ActivatedRoute
  ) {  }

  ngOnInit() {
    this.imagePath =  this.route.snapshot.paramMap.get('imgPath');
    
  }

}
