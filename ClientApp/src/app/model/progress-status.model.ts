import { ImageInfo } from '../imageinfo';
export interface ProgressStatus{
    status: ProgressStatusEnum;
    percentage?: number;
    imageInfo?: ImageInfo;
}

export enum ProgressStatusEnum {
    START, 
    COMPLETE, 
    IN_PROGRESS, 
    ERROR
}