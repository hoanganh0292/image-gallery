﻿using MongoDB.Driver;
using ImageGallery.Models;

namespace ImageGallery.Interface
{
    public interface IBookStoreContext
    {
        IMongoDatabase BookStoreDatabase { get; }
    }
}
