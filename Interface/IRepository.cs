﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageGallery.Models;
using MongoDB.Bson;
namespace ImageGallery.Interface
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> Get(string id);
        Task<BsonValue> Add(T item);
        Task<bool> Update(string id, T item);
        Task<bool> Remove(string id);            
    }
}
