﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageGallery
{
    public class ImageInfo
    {
        public string Path { get; set; }
        public string Title { get; set; }
        public string NameWithExtension { get; set; }
    }
}
