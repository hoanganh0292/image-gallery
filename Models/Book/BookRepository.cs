﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using MongoDB.Driver;
using MongoDB.Bson;
using ImageGallery.Interface;
using ImageGallery.Models;

namespace ImageGallery.Models
{
    public class BookRepository : IRepository<Book>
    {
        private IMongoCollection<Book> _bookCollection;
        private IBookStoreContext _bookStoreContext;
        public BookRepository(IBookStoreContext bookStoreContext)
        {
            _bookCollection = bookStoreContext.BookStoreDatabase.GetCollection<Book>("Book");
            _bookStoreContext = bookStoreContext;
        }
        public async Task<Book> Get(string id)
        {
            return await _bookCollection.Find(Builders<Book>.Filter.Eq(x => x.Id, id))
                                        .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Book>> GetAll()
        {
            return await _bookCollection.Find(_ => true).ToListAsync();
        }

        /// <summary>
        /// Add Book item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<BsonValue> Add(Book item)
        {
            var bookCollection = _bookStoreContext.BookStoreDatabase.GetCollection<BsonDocument>("Book");
            var document = new BsonDocument();
            document.Add("Name", item.BookName)
                    .Add("Price", item.Price)
                    .Add("Category", item.Category)
                    .Add("Author", item.Author)
                    .Add("Size", new BsonDocument {
                                { "Height", item.Size.Height },
                                { "Width", item.Size.Width},
                                {"Uom", item.Size.Uom }
                                }
                        );
            var promotionsBson = new BsonArray();
            foreach (var promotion in item.Promotions)
            {
                promotionsBson.Add(promotion.ToBsonDocument());
            }
            

            document.Add("Promotions", promotionsBson);

            var displayValues = new BsonDocument();
            foreach (var promotion in item.Promotions)
            {
                displayValues.Add(promotion.Name, promotion.Color);
            }
            document.Add("DisplayValues", displayValues);

            await bookCollection.InsertOneAsync(document);
            return document["_id"];
        }

        public Task<bool> Update(string id, Book item)
        {
            throw new NotImplementedException();
        }
        public Task<bool> Remove(string id)
        {
            throw new NotImplementedException();
        }
    }
}
