﻿using ImageGallery.Interface;
using ImageGallery.Models;
using MongoDB.Driver;

namespace ImageGallery.Services
{
    public class BookStoreContext : IBookStoreContext
    {        
        private readonly IMongoDatabase _bookStoreDB;
        public BookStoreContext(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _bookStoreDB = client.GetDatabase(settings.DatabaseName);            
        }

        public IMongoDatabase BookStoreDatabase => _bookStoreDB;
    }
}
