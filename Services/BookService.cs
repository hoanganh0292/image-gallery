﻿using ImageGallery.Models;
using MongoDB.Driver;
using System.Linq;
using System.Collections.Generic;
using ImageGallery.Interface;
using System.Threading.Tasks;
using ClosedXML.Report;

namespace ImageGallery.Services
{
    public class BookService
    {
        private readonly IMongoCollection<Book> _books;
        private readonly IRepository<Book> _bookRepository;
        public BookService(IBookstoreDatabaseSettings settings, IRepository<Book> bookRepository)
        {
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);
            _books = db.GetCollection<Book>("Book");
            _bookRepository = bookRepository;
        }

        public List<Book> Get() => _books.Find(book => true).ToList();

        public Book Get(string id) => _books.Find(book => book.Id == id).FirstOrDefault();

        public async Task<ResponseResult> Create(Book book)
        {
            var result = await _bookRepository.Add(book);
            return new ResponseResult()
            {
                Success = true,
                Value = result
            };            
            //_books.InsertOne(book);
        }

        public void Update(string id, Book bookInput)
        {
            _books.ReplaceOne(book => book.Id == id, bookInput);
            //System.Func<Book, bool> xz = delegate (Book book) { return book.Id == id; };
            //FilterDefinition<Book> x = xz;
        }
        public void Remove(string id) => _books.DeleteOne(book => book.Id == id);

        public async Task<Book> GetDataToExport(string id)
        {
            var book = await _bookRepository.Get(id);
            using (var template = new XLTemplate(@"..\ImageGallery\ExportTemplates\ChannelBudget.xlsx")) 
            {
                template.AddVariable(book);
                template.Generate();
                template.Workbook.Worksheet(1).Rows().AdjustToContents();
                template.SaveAs(@"..\ImageGallery\wwwroot\ChannelBudgetExport.xlsx");
            }
            return book;
        }

    }
}
