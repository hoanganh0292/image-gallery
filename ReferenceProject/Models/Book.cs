﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ReferenceProject.Models
{
    public class Book: BaseMongoDBEntity
    {        
        [BsonElement("Name")]
        [JsonProperty("Name")]
        public string BookName { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public string Author { get; set; }
        public BookSize Size { get; set; }       
        public IEnumerable<Promotion> Promotions { get; set; }
    }
    public class BookSize
    {
        public double Height { get; set; }
        public double Width { get; set; }
        public string Uom { get; set; }
    }

    public class Promotion: BaseMongoDBEntity
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Color { get; set; }
    }
}
