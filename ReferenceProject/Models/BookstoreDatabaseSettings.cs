﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ReferenceProject.Models
{
    public class BookstoreDatabaseSettings: IBookstoreDatabaseSettings
    {
        private ILogger<BookstoreDatabaseSettings> _logger;
        public BookstoreDatabaseSettings(IConfiguration configuration, ILogger<BookstoreDatabaseSettings> logger)
        {
            _logger = logger;
            ConnectionString = configuration.GetSection("BookstoreDatabaseSettings:ConnectionString").Value;
            DatabaseName = configuration.GetSection("BookstoreDatabaseSettings:DatabaseName").Value;
            _logger.LogInformation($"MongoDB connection infomation: {ConnectionString} /n {DatabaseName}");
        }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IBookstoreDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
