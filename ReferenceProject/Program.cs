﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using ReferenceProject.Models;
using Microsoft.Extensions.Logging;
using ReferenceProject.Migration;

namespace ReferenceProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile($"appsettings.{environmentName}.json", true);
                
            var configuration = configurationBuilder.Build();
            
            var sqlConn = configuration.GetConnectionString("SqlConnection");
            var mongoConn = configuration.GetSection("BookstoreDatabaseSettings:ConnectionString").Value;
            var mongoDB = configuration.GetSection("BookstoreDatabaseSettings:DatabaseName").Value;
            
            Console.WriteLine($"Connect string: {sqlConn} \n {mongoConn} \n {mongoDB}");            
            
            var serviceProvider = new ServiceCollection()
                                            .AddSingleton<IConfiguration>(configuration)
                                            .AddSingleton(typeof(ILogger<>), typeof(Logger<>))
                                            .AddLogging()
                                            .AddSingleton<IBookstoreDatabaseSettings, BookstoreDatabaseSettings>()
                                            .AddTransient<ICustomMigration, PromotionMigration>()
                                            .AddTransient<ICustomMigration, BookMigration>()
                                            .BuildServiceProvider();
            
            var _logger = serviceProvider.GetService<ILogger<Program>>();
            _logger.LogInformation("Test log");
                        
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("ReferenceProject.Program", LogLevel.Debug);                   
            });

            var lstMigration = serviceProvider.GetServices<ICustomMigration>();
            foreach (var migration in lstMigration)
            {
                migration.CreateData();
            }
        }
    }
}
