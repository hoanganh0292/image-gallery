﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using ReferenceProject.Models;
namespace ReferenceProject.Migration
{
    class PromotionMigration: ICustomMigration
    {
        private IMongoDatabase _bookDatabase;
        public PromotionMigration(IBookstoreDatabaseSettings bookstoreDatabaseSettings)
        {            
            var clientDB = new MongoClient(bookstoreDatabaseSettings.ConnectionString);
            _bookDatabase = clientDB.GetDatabase(bookstoreDatabaseSettings.DatabaseName);                        
        }
        
        public int CreateData()
        {
            var promotionCollection = _bookDatabase.GetCollection<BsonDocument>("Promotion");
            if (promotionCollection.EstimatedDocumentCount() <= 0)
            {
                var lstPromotion = new List<BsonDocument>();
                var doc = new BsonDocument();
                doc.Add("Name", "Bookmark");
                doc.Add("Quantity", 100);
                doc.Add("Color", "Red");
                lstPromotion.Add(doc);
                
                Dictionary<string, object> diDoc = new Dictionary<string, object>();
                diDoc.Add("Name", "Photo frame");
                diDoc.Add("Quantity", 50);
                diDoc.Add("Color", "Blue");                
                lstPromotion.Add(new BsonDocument(diDoc));

                lstPromotion.Add(new BsonDocument {
                    {"Name", "Postcard"},
                    {"Quantity",70 },
                    {"Color", "Gray" }
                });

                BsonDocument bDoc;
                if(BsonDocument.TryParse("{Name: 'Scratch card', Quantity: 120, Color: ''}", out bDoc))
                {
                    lstPromotion.Add(bDoc);
                }

                promotionCollection.InsertMany(lstPromotion);
                Console.WriteLine("Migrate promotion data success.");
            }
            else
                Console.WriteLine("Promotion data existed.");
            return 0;
            
        }
    }
}
