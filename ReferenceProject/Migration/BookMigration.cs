﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using ReferenceProject.Models;
namespace ReferenceProject.Migration
{
    class BookMigration: BaseMigration, ICustomMigration
    {
        public BookMigration(IBookstoreDatabaseSettings bookstoreDatabaseSettings): base(bookstoreDatabaseSettings)
        {

        }

        public int CreateData()
        {
            var bookCollection = _bookDatabase.GetCollection<BsonDocument>("Book");
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.Eq("Category", "Novel");
            if (bookCollection.Find(filter).FirstOrDefault() == null )
            {
                var promotion = new BsonArray
                {
                    new BsonDocument
                    {
                        {"Name", "Bookmark" },
                        {"Quantity", 1 },
                        {"Color", "Blue" }
                    }
                };
                bookCollection.UpdateMany(x => true, Builders<BsonDocument>.Update.Set("Promotions", promotion));

                var book = GetBookWithPromotionExample();
                bookCollection.InsertOne(book);

                Console.WriteLine("Migrate book success");
            }
            return 0;
        }
        public BsonDocument GetBookWithPromotionExample()
        {
            var doc = new BsonDocument();
            doc.Add("Name", "When breath become air");
            doc.Add("Price", 20.4);
            doc.Add("Category", "Novel");
            doc.Add("Author", "Paul Kalanithi");
            doc.Add("Size", new BsonDocument {
                    { "Height", 8.0 },
                    { "Width", 6.0},
                    {"Uom", "inch" }
                });
            doc.Add("Promotions", new BsonArray
                {
                    new BsonDocument{
                        {"Name", "Bookmark" },
                        {"Quantity", 2 },
                        {"Color", "Red" }
                    },
                    new BsonDocument{
                        {"Name", "Postcard" },
                        {"Quantity", 1 },
                        {"Color", "Grey" }
                    }
                });
            return doc;
        }
    }
}
