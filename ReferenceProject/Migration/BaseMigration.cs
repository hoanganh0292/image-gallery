﻿using MongoDB.Driver;
using ReferenceProject.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReferenceProject.Migration
{
    public abstract class BaseMigration
    {
        public IMongoDatabase _bookDatabase { get; }
        public BaseMigration(IBookstoreDatabaseSettings bookstoreDatabaseSettings)
        {
            var clientDB = new MongoClient(bookstoreDatabaseSettings.ConnectionString);
            _bookDatabase = clientDB.GetDatabase(bookstoreDatabaseSettings.DatabaseName);
        }
    }
}
