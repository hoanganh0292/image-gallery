﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReferenceProject.Migration
{
    interface ICustomMigration
    {
        int CreateData();
    }
}
