﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReferenceProject
{
    class MongoDatabaseSetting: IMongoDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
    public interface IMongoDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
