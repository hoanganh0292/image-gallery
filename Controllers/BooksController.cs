﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ImageGallery.Services;
using ImageGallery.Models;

namespace ImageGallery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        BookService _bookService;
        public BooksController(BookService bookService)
        {
            _bookService = bookService;
        }
        
        /// <summary>
        /// Get all book in repository
        /// </summary>
        /// <returns></returns>
        /// <response code="201">Execute success</response>
        [HttpGet]
        public ActionResult<List<Book>> Get() =>
            _bookService.Get();
        
        /// <summary>
        /// Get book by id
        /// </summary>
        /// <param name="id"></param>        
        [HttpGet("{id:length(24)}", Name = "GetBook")]
        public ActionResult<Book> Get(string id)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            return book;
        }

        [HttpPost]
        public async Task<IActionResult> Create(Book book)
        {
            var result = await _bookService.Create(book);
            if (result.Success)
                return Ok(result);
            return BadRequest(result);
            //return Ok("GetBook", new { id = book.Id.ToString() }, book);
        }

        [HttpGet("export/book/{id:length(24)}", Name = "ExportBookData")]
        public async Task<IActionResult> Export (string id)
        {
            var result = await _bookService.GetDataToExport(id);
            return Ok(result);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Book bookIn)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookService.Update(id, bookIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookService.Remove(book.Id);

            return NoContent();
        }
    }
}
