﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ImageGallery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private IHostingEnvironment _hostingEnvironment;
        private string _galleryPath;
        const string DIRECTORY_GALLERY_PATH = @"\gallery\";
        public ImageController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _galleryPath = Path.Combine(_hostingEnvironment.WebRootPath, "gallery");
        }

        // GET: api/Image
        [HttpGet]
        public IEnumerable<ImageInfo> Get()
        {            
            if (!Directory.Exists(_galleryPath))
            {
                return null;
            }
            var searchPattern = new string[] { "jpg", "png", "svg" };
            List<string> lstFilePath = new List<string>();
            foreach (string filter in searchPattern)
                lstFilePath.AddRange(Directory.GetFiles(_galleryPath, $"*.{filter}", SearchOption.TopDirectoryOnly));
            return GetListImageInfoByPaths(lstFilePath);
        }

        // GET: api/Image/name
        [HttpGet("{keyword}", Name = "GetByKeyword")]
        public IEnumerable<ImageInfo> GetImageByKeyword(string keyword)
        {
            if (!Directory.Exists(_galleryPath))
            {
                 return null;
            }
            var searchPattern = new string[] { "jpg", "png", "svg" };
            List<string> lstFilePath = new List<string>();
            foreach (string filter in searchPattern)
                lstFilePath.AddRange(Directory.GetFiles(_galleryPath, $"*.{filter}", SearchOption.TopDirectoryOnly)                                                                         
                                              .Where(x => string.IsNullOrEmpty(keyword.ToString()) 
                                                          || Path.GetFileNameWithoutExtension(x).Contains(keyword, StringComparison.OrdinalIgnoreCase))
                                    );
             return GetListImageInfoByPaths(lstFilePath);
        }

        private IEnumerable<ImageInfo> GetListImageInfoByPaths(List<string> lstFilePath)
        {
            lstFilePath.Sort();
            foreach (string path in lstFilePath)
            {
                FileInfo fi = new FileInfo(path);
                yield return new ImageInfo
                {
                    Path = DIRECTORY_GALLERY_PATH + fi.Name,
                    Title = fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length),
                    NameWithExtension = fi.Name
                };
            }
        }
        // POST: api/Image
        [HttpPost]
        public async Task<IActionResult> Upload()
        {
            try
            {                
                if (!Directory.Exists(_galleryPath))
                {
                    Directory.CreateDirectory(_galleryPath);
                }
                                
                if (Request.Form.Files.Count > 0 && Request.Form.Files[0].Length > 0)
                {
                    var img = Request.Form.Files[0];
                    var fileName = img.FileName;
                    var filePath = Path.Combine(_galleryPath, fileName);
                    if (System.IO.File.Exists(filePath))
                    {
                        fileName = Path.GetFileNameWithoutExtension(filePath) + "_" + DateTime.Now.ToString("hms_dMyy") + Path.GetExtension(filePath);
                        filePath = Path.Combine(_galleryPath, fileName);
                    }                        

                    using (var fileStream = new FileStream(filePath, FileMode.CreateNew))
                    {                        
                        await img.CopyToAsync(fileStream);
                    }                    
                    var imgData = new ImageInfo()
                    {
                        Path = DIRECTORY_GALLERY_PATH + fileName,
                        Title = Path.GetFileNameWithoutExtension(filePath),
                        NameWithExtension = fileName
                    };
                    return Ok(imgData);
                }
                else
                    return Ok(new ImageInfo());
                
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }            
        }

        // PUT: api/image
        [HttpPut()]
        public ActionResult UpdateImageName([FromBody] object body)
        {
            try
            {
                var imageData = JsonConvert.DeserializeObject<Dictionary<string, string>>(body.ToString());                
                var oldImageNameWithExtension = imageData["OldName"];
                var newImageName = imageData["NewName"];
                var imgFullPathOld = Path.Combine(_galleryPath, oldImageNameWithExtension);
                if (System.IO.File.Exists(imgFullPathOld))
                {
                    var imgFullPathNew = Path.Combine(_galleryPath, newImageName + Path.GetExtension(imgFullPathOld));
                    if (!System.IO.File.Exists(imgFullPathNew))
                    {
                        System.IO.File.Move(imgFullPathOld, imgFullPathNew);
                        return Ok();
                    }                        
                    else
                        return BadRequest(new { message = $"File '{newImageName}' existed. Please choose another name." });
                }
                else
                {
                    return BadRequest(new { message = $"File '{oldImageNameWithExtension}' not existed, please check again." });
                }
                
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }

        }

        // DELETE: api/image
        [HttpDelete]
        public ActionResult Delete([FromBody] object body)
        {
            try
            {
                var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(body.ToString());
                var imagePath = Path.Combine(_galleryPath, data["ImageName"].ToString());
                if (System.IO.File.Exists(imagePath))
                {
                    System.IO.File.Delete(imagePath);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
